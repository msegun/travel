<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Illuminate\Support\Facades\Auth;

class DashboardController extends \App\Http\Controllers\AdminController
{
    
    public function index(){
        return view('admin.dashboard', [
            'page'=>'Dashboard',
            'user'=> Auth::guard($this->guard)->user(),
            'active'=> 'dashboard',
            'breadcumb'=>[
//                [
//                    'url'=>'',
//                    'active'=>true,
//                    'title'=>''
//                ]
            ]
        ]);
    }
    
}
