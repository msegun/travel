<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Config;

class SiteController extends \App\Http\Controllers\AdminController
{
    //
    protected $page_group_title = "Site configuration";
    private $success_message = null;
    
    private function _profile_manager(Request $request){
        $config_data = [];
        if($request->isMethod("POST")){
            $config_data = $request->except(['_token']);
            if(!isset($config_data['site_multi_currency'])){$config_data['site_multi_currency']=0;}
            foreach ($config_data as $key=>$value){
                //var_dump($key." ".$value);
                Config::updateOrCreate(['k'=>$key], ['v'=>$value]);
            }
            $this->success_message = "Configuration updated successfully";
        }
        else{
            $db_data = Config::all();
            foreach ($db_data->toArray() as $value){
                $config_data[$value['k']] = $value['v'];
            }
            
        }
        return $config_data;
    }
    
    public function profile(Request $request){
        $config_data = $this->_profile_manager($request);
        $can_complete_config = empty($config_data)?false:true;
        return view('admin.site.profile', [
            'page'=>'Website Profile',
            'user'=> Auth::guard($this->guard)->user(),
            'active'=> 'site',
            'success'=> $this->success_message,
            'data'=> $config_data,
            'can_complete_config'=> $can_complete_config,
            'breadcumb'=>[
                [
                    'url'=>'javascript:;',
                    'active'=>false,
                    'title'=> $this->page_group_title
                ],
                [
                    'url'=>'javascript:;',
                    'active'=>true,
                    'title'=>"Profile"
                ]
            ]
        ]);
    }
    
    public function content(Request $request, $tab=1){
        $config_data = $this->_content_manager($request, $tab);
        $can_store_show_now = array_key_exists('setup_done', $config_data)?true:false;
        return view('admin.site.content', [
            'page'=>'Website Content',
            'user'=> Auth::guard($this->guard)->user(),
            'active'=> 'site',
            'success'=> $this->success_message,
            'data'=> $config_data,
            'tab'=> $tab,
            'can_store_show_now'=> $can_store_show_now,
            'breadcumb'=>[
                [
                    'url'=>'javascript:;',
                    'active'=>false,
                    'title'=> $this->page_group_title
                ],
                [
                    'url'=>'javascript:;',
                    'active'=>true,
                    'title'=>"Content"
                ]
            ]
        ]);
    }
    
    private function _content_manager(Request $request, $tab){
        switch ($tab){
            case 1:
                if($request->isMethod('POST')){
                    $logo_post = $request->input('site_logo');
                    $logo_array = explode(',', $logo_post);
                    $extension = explode('/', explode(':', explode(';', $logo_array[0])[0])[1])[1];
                    $logo = $logo_array[1];
                    $file_name = 'logo.'.$extension;
                    file_put_contents(public_path().'/uploads/'.$file_name, base64_decode($logo));
                    //then save the file_name to config
                    Config::updateOrCreate(['k'=>'site_logo'], ['v'=>$file_name]);
                    $this->success_message = "Logo updated successfully";
                    return ['site_logo'=> $file_name];
                }
                else{
                    return ['site_logo'=> Config::find('site_logo', ['v'])->v];
                }
                break;
            default:
                return [];
        }
        return [];
    }
}
