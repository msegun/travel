<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    protected $guard = 'admin';
    
    public function __construct() {
        $this->middleware($this->guard);
    }
    
    protected function guard(){
        return Auth::guard($this->guard);
    }
}
