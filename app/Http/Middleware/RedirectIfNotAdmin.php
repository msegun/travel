<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
//        var_dump(Auth::guard()->check());
//        var_dump($request->session()->all());die;
        if (!Auth::guard('admin')->check()) {
            return redirect('/');
        }
        return $next($request);
    }
}
