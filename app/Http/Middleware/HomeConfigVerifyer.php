<?php

namespace App\Http\Middleware;

use Closure;
use App\Config;

class HomeConfigVerifyer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $configurations = [];
        //pull configurations
        $configs = Config::all(['k','v'])->toArray();
        foreach ($configs as $value){
            $configurations[$value['k']] = $value['v'];
        }
        if(!array_key_exists('config_done', $configurations)
                && ($request->path() != 'setup') 
                && (!starts_with($request->path(), 'admin'))){
            return redirect('/setup');
        }
        //push this to the requester
        return $next($request);
    }
}
