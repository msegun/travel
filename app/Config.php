<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Config extends Model
{
    use SoftDeletes;
    
    protected $primaryKey = 'k';
    protected $fillable = ['k', 'v'];
    public $incrementing = false;
}
