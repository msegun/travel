@extends('layouts.admin')
  
@section('body')
    <style type="text/css">
      body {
        background-color: #DADADA;
      }
      body > .grid {
        height: 100%;
      }
      .image {
        margin-top: -100px;
      }
      .column {
        max-width: 450px;
      }
    </style>
    <script>
    $(document)
      .ready(function() {
        $('.ui.form')
          .form({
            fields: {
              email: {
                identifier  : 'email',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Please enter your e-mail'
                  },
                  {
                    type   : 'email',
                    prompt : 'Please enter a valid e-mail'
                  }
                ]
              },
              password: {
                identifier  : 'password',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Please enter your password'
                  },
                  {
                    type   : 'length[6]',
                    prompt : 'Your password must be at least 6 characters'
                  }
                ]
              }
            }
          });
      });
    </script>
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui teal image header">
              <img src="{{ asset('img/logo-invert.png') }}" class="image">
              <div class="content">
                Admin
              </div>
            </h2>
            <form class="ui large form" method="POST" action="/admin/login">
                <div class="ui negative message {{$errors->has('email')?'show':'hidden'}}"  style="text-align:left;">
                  <i class="close icon msg-close"></i>
                  <div class="header">Error</div>
                  <p>
                    {{$errors->has('email')?$errors->first('email'):''}}
                  </p>
                </div>
                {{ csrf_field() }}
                <div class="ui stacked segment">
                  <div class="field {{$errors->has('email')?'error':''}}">
                    <div class="ui left icon input">
                      <i class="user icon"></i>
                      <input type="email" name="email" placeholder="E-mail address">
                    </div>
                  </div>
                  <div class="field {{$errors->has('password')?'error':''}}">
                    <div class="ui left icon input">
                      <i class="lock icon"></i>
                      <input type="password" name="password" placeholder="Password">
                    </div>
                  </div>
                  <div class="ui fluid large teal submit button">Login</div>
                </div>

                <div class="ui error message"></div>
            </form>
            <div class="ui message">
              Forgot password ? <a href="/admin/reset-password">Reset password here</a>
            </div>
            <div class="ui message text-center">
                <a href="/"><i class="home icon bordered"></i></a>
                <a href="/register-login"><i class="user icon bordered"></i></a>
            </div>
        </div>
    </div>
@endsection