@extends('layouts.admin_dashboard')

    @section('board')
        @if($can_complete_config)
            <div class="ui warning message">
                <i class="close icon msg-close"></i>
                <div class="header">Configuration Okay</div>
                <p>
                  You can now modify Site CMS. You can also update these configurations at any time. 
                </p>
            </div>
        @endif
        <div class="ui icon info message">
          <i class="save icon"></i>
          <div class="content">
            <div class="header">
              Update configuration profile
            </div>
            <p>This sets some features and values for the front website used by customers.</p>
          </div>
        </div>
        <form method="POST" class="ui form">
            
            <div class="ui positive message {{$success?'show':'hidden'}}">
              <i class="close icon msg-close"></i>
              <div class="header">Success</div>
              <p>
                {{$success}}
              </p>
            </div>

            {{csrf_field()}}
              <div class="two fields">
                <div class="field required">
                  <label>Company Title</label>
                  <input placeholder="Demo Travel Agency LTD" type="text" name="company_title" value="{{isset($data['company_title'])?$data['company_title']:''}}">
                </div>
                <div class="field required">
                  <label>Company Keywords (separate words with comma)</label>
                  <input placeholder="Separate SEO Keywords with comma" type="text" name="company_keywords" value="{{isset($data['company_keywords'])?$data['company_keywords']:''}}">
                </div>
              </div>
            <div class="field">
              <label>Company Description</label>
              <textarea rows="2" name="company_description">{{isset($data['company_description'])?$data['company_description']:''}}</textarea>
            </div>
              <div class="two fields">
                <div class="field">
                  <label>Front Site Layout</label>
                    <div class="ui selection dropdown">
                        <input name="site_layout" type="hidden" value="{{isset($data['site_layout'])?$data['site_layout']:'1'}}">
                        <i class="dropdown icon"></i>
                        <div class="default text">Default (Classic)</div>
                        <div class="menu">
                            <div class="item" data-value="1">Classic</div>
                            <div class="item" data-value="2">Pro Classic</div>
                        </div>
                    </div>
                </div>
                <div class="field">
                  <label>Front Site Color Theme</label>
                    <div class="ui selection dropdown">
                        <input name="site_theme" type="hidden" value="{{isset($data['site_theme'])?$data['site_theme']:'brick-red'}}">
                        <i class="dropdown icon"></i>
                        <div class="default text">Default (brick-red)</div>
                        <div class="menu">
                            <div class="item" data-value="brick-red">brick-red</div>
                            <div class="item" data-value="bright-turquoise">bright-turquoise</div>
                            <div class="item" data-value="cerise">cerise</div>
                            <div class="item" data-value="de-york">de-york</div>
                            <div class="item" data-value="denim">denim</div>
                            <div class="item" data-value="green-smoke">green-smoke</div>
                            <div class="item" data-value="hippie-blue">hippie-blue</div>
                            <div class="item" data-value="horizon">horizon</div>
                            <div class="item" data-value="leather">leather</div>
                            <div class="item" data-value="mandy">mandy</div>
                            <div class="item" data-value="salem">salem</div>
                            <div class="item" data-value="scarlet">scarlet</div>
                            <div class="item" data-value="shamrock">shamrock</div>
                            <div class="item" data-value="studio">studio</div>
                            <div class="item" data-value="turkish-rose">turkish-rose</div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="two fields">
                <div class="field">
                  <label>Default Currency</label>
                    <div class="ui selection dropdown">
                        <input name="site_currency" type="hidden" value="{{isset($data['site_currency'])?$data['site_currency']:'NGN'}}">
                        <i class="dropdown icon"></i>
                        <div class="default text">Default (NGN)</div>
                        <div class="menu">
                            <div class="item" data-value="NGN">NGN</div>
                            <div class="item" data-value="USD">USD</div>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <label>Allow Multi-Currency ?</label>
<!--                    <div class="inline field fitted">-->
                        
                      <div class="ui toggle checkbox ">
                          @if(isset($data['site_multi_currency']))
                            @if($data['site_multi_currency'])
                                <input name="site_multi_currency" class="hidden" type="checkbox" value="1" checked>
                            @else
                                <input name="site_multi_currency" class="hidden" type="checkbox" value="1">
                            @endif
                          @else
                            <input name="site_multi_currency" class="hidden" type="checkbox" value="1">
                          @endif
                        <label>Yes </label>
                      </div>
<!--                    </div>-->
                </div>
            </div>
            <div class="field">
                <div class="ui large teal submit button"><i class="icon save"></i> Save</div>
            </div>
            <div class="ui error message"></div>
        </form>
    <script>
    $(document)
      .ready(function() {
        $('.ui.form')
          .form({
            fields: {
              company_title: ['minLength[6]', 'empty'],
              company_keywords: ['minLength[6]', 'empty']
            }
          });
      });
    </script>
    @endsection