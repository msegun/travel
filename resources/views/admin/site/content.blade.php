@extends('layouts.admin_dashboard')
    @push('css')
        <link href="{{ asset('cropper/cropper.min.css') }}" rel="stylesheet">
    @endpush
    @push('scripts')
        <script src="{{ asset('cropper/cropper.min.js') }}"></script>
    @endpush
    @section('board')
        @if($can_store_show_now)
            <div class="ui warning message">
                <i class="close icon msg-close"></i>
                <div class="header">Site Ready</div>
                <p>
                  Everything your homepage. 
                </p>
            </div>
        @endif
        <div class="ui steps">
          <a class="{{$tab==1?'active':''}} step" href="/admin/site/content/1">
            <div class="content">
              <div class="title">Logo</div>
              <div class="description">Upload or Change Site Logo</div>
            </div>
          </a>
          <a class="{{$tab==2?'active':''}} step" href="/admin/site/content/2">
            <div class="content">
              <div class="title">Social</div>
              <div class="description">Enable/Disable and configure social links</div>
            </div>
          </a>
          <a class="{{$tab==3?'active':''}} step" href="/admin/site/content/3">
            <div class="content">
              <div class="title">Footer</div>
              <div class="description">CMS for the footer contents</div>
            </div>
          </a>
        </div>
        @if($tab == 1)
            <form method="POST" class="ui form">

                <div class="ui positive message {{$success?'show':'hidden'}}">
                  <i class="close icon msg-close"></i>
                  <div class="header">Success</div>
                  <p>
                    {{$success}}
                  </p>
                </div>

                {{csrf_field()}}
                <div class="ui right aligned grid">
                    <div class="left floated left aligned ten wide column">
                        <div class="fields two">
                            <div class="field">
                                <label>Choose a file and crop the logo before Save</label>
                                <div id="image_upload_control" class="ui input">
                                    <input type="file" accept="image/*" id="image_upload"/>
                                </div>
                                <input type="hidden" name='site_logo' id='logo_image' value="" />
                            </div>
                            <div class="field right floated">
                                <label>.</label>
                                <div class="ui icon buttons">
                                    <button class="ui button green disabled control-btn"><i class='icon check'></i></button>
                                </div>
                                <div class="ui icon buttons">
                                    <a href="" class="ui button red disabled control-btn"><i class='icon remove'></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="right floated center aligned six wide column">
                        <!-- current logo -->
                        <div class="field center">
                            <label>Current Logo</label>
                            <div class="ui small image bordered padded">
                              <svg width="110" height="40">
                              <image xlink:href="{{isset($data['site_logo'])?'/uploads/'.$data['site_logo']:'/img/logo-white.png'}}" y="0" width="100%" height="100%"></image>
                              </svg>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field" style="max-width: 750px; max-height: 400px">
                  <img id="image" src="">
                </div>
                <div class="ui error message"></div>
            </form>
        @endif
    <script>
        var $image = $('#image');
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var reader2 = new FileReader();
                reader2.onload = function (e) {
                    $('#logo_image').val(e.target.result);
                };
                reader.onload = function (e) {
                    $image.attr('src', e.target.result);
                    $('#image_upload_control').addClass('disabled');
                    $('.control-btn').removeClass('disabled');
                    $image.cropper({
                      aspectRatio: 2.75 / 1,
                      crop:function(ev){
                        $image.cropper('getCroppedCanvas').toBlob(function (blob) {
                            reader2.readAsDataURL(blob);
                        });
                      }
                    });
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(function () {
            $("#image_upload").on("change", function(e){
                readURL(this);
                //$(this).hide(620);
            });
        });
    </script>
    @endsection