@extends('layouts.front')

@section('body')
    @include('includes.ads_top')
    <div class="gap"></div>
    <example id="app"></example>
    @include('includes.company_core')
    @include('includes.deal_ad')
    @include('includes.available_deals')
@endsection
