<div class="top-user-area clearfix">
    <ul class="top-user-area-list list list-horizontal list-border">
        <li class="nav-drop"><a href="#">NGN &#8358;<i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i></a>
            <ul class="list nav-drop-menu">
                <li><a href="#">USD<span class="right">$</span></a>
                </li>
            </ul>
        </li>
        @if(Auth::guard('web')->check())
            <li class="top-user-area-avatar">
                <a href="user-profile.html">
                    <img class="origin round" src="img/40x40.png" alt="Image Alternative text" title="AMaze" />Hi, John</a>
            </li>
            <li><a href="#">Sign Out</a></li>
        @else
            <li><a href="/register-login">Register / Login</a></li>
        @endif
    </ul>
</div>