<!-- TOP AREA -->
<div class="top-area show-onload">
    <div class="bg-holder full">
        <div class="bg-front full-height bg-front-mob-rel">
            <div class="container full-height">
                <div class="rel full-height">
                    <div class="tagline visible-lg" id="tagline"><span>It's time to</span>
                        <ul>
                            <li>live</li>
                            <li>have fun</li>
                            <li>relax</li>
                            <li>meet</li>
                            <li>being lost</li>
                            <li>run away</li>
                            <li>go</li>
                            <li>explore</li>
                            <li>find new friends</li>
                            <li class="active">discover</li>
                            <li>play</li>
                        </ul>
                    </div>
                    @include('includes.searchbox')
                </div>
            </div>
        </div>
        <div class="owl-carousel owl-slider owl-carousel-area visible-lg" id="owl-carousel-slider" data-nav="false">
            <div class="bg-holder full">
                <div class="bg-mask"></div>
                <div class="bg-img" style="background-image:url(img/2048x1365.png);"></div>
            </div>
            <div class="bg-holder full">
                <div class="bg-mask"></div>
                <div class="bg-img" style="background-image:url(img/2048x2048.png);"></div>
            </div>
            <div class="bg-holder full">
                <div class="bg-mask"></div>
                <div class="bg-img" style="background-image:url(img/2048x1365.png);"></div>
            </div>
        </div>
        <div class="bg-img hidden-lg" style="background-image:url(img/2048x1365.png);"></div>
        <div class="bg-mask hidden-lg"></div>
    </div>
</div>
<!-- END TOP AREA  -->