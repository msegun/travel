<div class="bg-holder">
    <div class="bg-mask"></div>
    <div class="bg-img" style="background-image:url(img/2048x1293.png);"></div>
    <div class="bg-content">
        <div class="container">
            <div class="gap gap-big text-center text-white">
                <h2 class="text-uc mb20">Last Minute Deal</h2>
                <ul class="icon-list list-inline-block mb0 last-minute-rating">
                    <li><i class="fa fa-star"></i>
                    </li>
                    <li><i class="fa fa-star"></i>
                    </li>
                    <li><i class="fa fa-star"></i>
                    </li>
                    <li><i class="fa fa-star"></i>
                    </li>
                    <li><i class="fa fa-star"></i>
                    </li>
                </ul>
                <h5 class="last-minute-title">Lagos - New York</h5>
                <p class="last-minute-date">Fri 14 Aug - Sun 16 Nov</p>
                <p class="mb20"><b>{{config('product.currency_code')}} 120</b> / person</p><a class="btn btn-lg btn-white btn-ghost" href="#">Book Now <i class="fa fa-angle-right"></i></a>
            </div>
        </div>
    </div>
</div>
