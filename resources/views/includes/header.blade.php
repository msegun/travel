<header id="main-header">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <a class="logo" href="/">
                        <img src="img/logo-invert.png" alt="Image Alternative text" title="Image Title" />
                    </a>
                </div>
                <div class="col-md-3 col-md-offset-2">
                    <p class='text-center'>Company Slogan or Info here</p>
                </div>
                <div class="col-md-4">
                    @include('includes.menu')
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="nav">
            <ul class="slimmenu" id="slimmenu">
                <li class="active"><a href="/">Home</a></li>
                <li class=""><a href="/about-us">About Us</a></li>
                <li class=""><a href="/packages">Packages</a></li>
                <li class=""><a href="/deals">Deals</a></li>
                <li class=""><a href="/tours">Travels &amp; Tours</a></li>
            </ul>
        </div>
    </div>
</header>
