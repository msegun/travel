<div class="ui vertical inverted sticky menu fixed top" style="height:100%; left: 0px; width: 200px ! important; margin-top: 0px; overflow: auto;">
<p class="item">
    <em>Hi</em> <span>{{$user->name}}</span>  <strong><small>({{$user->email}})</small></strong>
</p>
  <a class="item" href="/admin/dashboard">
      <i class="icon home"></i> Dashboard
  </a>
  <div class="item {{$active == 'site'?'active':''}}">
      <i class="icon cogs"></i> Site configuration
      <div class="menu">
          <a class="item" href="/admin/site/profile">Profile</a>
          <a class="item" href="/admin/site/content">Content</a>
      </div>
  </div>
  <div class="item {{$active == 'customers'?'active':''}}">
      <i class="icon users"></i> Customers
      <div class="menu">
          <a class="item" href="">Add</a>
          <a class="item" href="">Manage</a>
      </div>
  </div>
  <div class="item {{$active == 'deals'?'active':''}}">
      <i class="icon money"></i> Deals
      <div class="menu">
          <a class="item" href="">Add</a>
          <a class="item" href="">Manage</a>
      </div>
  </div>
  <div class="item {{$active == 'travels'?'active':''}}">
      <i class="icon travel"></i> Travels
      <div class="menu">
          <a class="item" href="">Add</a>
          <a class="item" href="">Manage</a>
      </div>
  </div>
  <div class="item {{$active == 'integrations'?'active':''}}">
      <i class="icon external"></i> Integrations
      <div class="menu">
          <a class="item" href="">Travelport</a>
          <a class="item" href="">Paystack</a>
      </div>
  </div>
    <div class="item aligned center">
      <a class="ui right negative labeled icon button mini" href="/admin/logout">
        <i class="left arrow icon"></i>
        Logout
      </a>
    </div>
</div>