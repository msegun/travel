<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row row-wrap text-center" data-gutter="120">
                <div class="col-md-4">
                    <div class="thumb">
                        <header class="thumb-header">
                            <i class="fa fa-briefcase box-icon-black round box-icon-big animate-icon-top-to-bottom centered_component"></i>
                        </header>
                        <div class="thumb-caption">
                            <h5 class="thumb-title"><a class="text-darken" href="#">Combine & Save</a></h5>
                            <p class="thumb-desc"></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumb">
                        <header class="thumb-header">
                            <i class="fa fa-thumbs-o-up box-icon-black round box-icon-big animate-icon-top-to-bottom centered_component"></i>
                        </header>
                        <div class="thumb-caption">
                            <h5 class="thumb-title"><a class="text-darken" href="#">Best Travel Agent</a></h5>
                            <p class="thumb-desc"></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumb">
                        <header class="thumb-header">
                            <i class="fa fa-send box-icon-black round box-icon-big animate-icon-top-to-bottom centered_component"></i>
                        </header>
                        <div class="thumb-caption">
                            <h5 class="thumb-title"><a class="text-darken" href="#">Best Destinations</a></h5>
                            <p class="thumb-desc"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="gap gap-small"></div>
</div>
