<!DOCTYPE HTML>
<html lang="{{ config('app.locale') }}">

<head>
    <title>{{config('product.owner')}} - {{$page}}</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="" />
    <meta name="description" content="{{config('product.description')}}">
    <meta name="author" content="{{config('product.author','')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icomoon.css') }}">
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/schemes/'.config('product.theme').'.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mystyles.css') }}">
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

</head>

<body>
    
    <div class="global-wrap">
        @include('includes.header')
            @yield('body')
        @include('includes.footer')
    </div>
    
    <script src="{{ asset('js/plugins/jquery.js') }}"></script>
    <script src="{{ asset('js/plugins/modernizr.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap.js') }}"></script>
    <script src="{{ asset('js/plugins/slimmenu.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('js/plugins/nicescroll.js') }}"></script>
    <script src="{{ asset('js/plugins/dropit.js') }}"></script>
    <script src="{{ asset('js/plugins/ionrangeslider.js') }}"></script>
    <script src="{{ asset('js/plugins/icheck.js') }}"></script>
    <script src="{{ asset('js/plugins/fotorama.js') }}"></script>
    <script src="{{ asset('js/plugins/typeahead.js') }}"></script>
    <script src="{{ asset('js/plugins/card-payment.js') }}"></script>
    <script src="{{ asset('js/plugins/magnific.js') }}"></script>
    <script src="{{ asset('js/plugins/owl-carousel.js') }}"></script>
    <script src="{{ asset('js/plugins/fitvids.js') }}"></script>
    <script src="{{ asset('js/plugins/tweet.js') }}"></script>
    <script src="{{ asset('js/plugins/countdown.js') }}"></script>
    <script src="{{ asset('js/plugins/gridrotator.js') }}"></script>
    <script src="{{ asset('js/plugins/custom.js') }}"></script>
    
</body>
</html>