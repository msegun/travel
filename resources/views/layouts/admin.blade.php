<!DOCTYPE HTML>
<html lang="{{ config('app.locale') }}">

<head>
    <title>Admin | {{config('product.owner')}} - {{$page}}</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="" />
    <meta name="description" content="{{config('product.description')}}">
    <meta name="author" content="{{config('product.author','')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="{{ asset('semantic/dist/semantic.min.css') }}" rel="stylesheet">
    <link href="{{ asset('semantic/dist/components/dropdown.min.css') }}" rel="stylesheet">
    <link href="{{ asset('semantic/dist/components/checkbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('semantic/dist/components/message.min.css') }}" rel="stylesheet">
    @stack('css')
    <script src="{{ asset('js/plugins/jquery.js') }}"></script>
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

</head>

<body>

    @yield('body')
    
    <script src="{{ asset('semantic/dist/semantic.min.js') }}"></script>
    <script src="{{ asset('semantic/dist/components/form.min.js') }}"></script>
    <script src="{{ asset('semantic/dist/components/transition.min.js') }}"></script>
    <script src="{{ asset('semantic/dist/components/dropdown.min.js') }}"></script>
    <script src="{{ asset('semantic/dist/components/checkbox.min.js') }}"></script>
    @stack('scripts')
    <script>
        $(document).ready(function(){
            $('.selection').dropdown();
            $('.ui.checkbox').checkbox();
            $('.msg-close').click(function(){
                $(this).closest('.message').transition('fade');
            });
        });
        
    </script>
</body>

</html>