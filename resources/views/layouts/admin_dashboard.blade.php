@extends('layouts.admin')

    @section('body')
        @include('includes.admin_sidebar')
        <div class="ui container-fluid " style="margin-left: 200px; height: 95%; overflow: auto;">
            <div class="ui small breadcrumb" style="margin-left: 10px">
                <a class="section" href="/admin/dashboard">Dashboard</a>
                @foreach($breadcumb as $oneb)
                    <i class="right chevron icon divider"></i>
                    <a class="section {{$oneb['active']?'active':''}}" href="{{$oneb['url']?$oneb['url']:'javascript:;'}}">{{$oneb['title']}}</a>
                @endforeach
            </div>
            <div class="ui container full-height">
                <div class="ui segment padded full-height" style="height: 100%;">
                    @yield('board')
                </div>
            </div>
        </div>
        <div class="ui inverted vertical footer segment padded sticky bottom">
            <div class="ui center aligned container">
                <p><small class="small">eTravel NG v1.0.0</small></p>
            </div>
        </div>
    @endsection
