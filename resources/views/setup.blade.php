@extends('layouts.admin')

@section('body')
    <style type="text/css">
      body {
        background-color: #DADADA;
      }
      body > .grid {
        height: 100%;
      }
      .image {
        margin-top: -100px;
      }
      .column {
        max-width: 450px;
      }
    </style>
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <div class="ui">
                <div class="content padded">
                    <div class="title">Incomplete Site Setup</div>
                    <p>Your <strong>Frontpage</strong> cannot open yet until you login as 
                        <a href="/admin/login">admin here</a> and complete 
                        <strong>necessary configurations</strong>.
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection

