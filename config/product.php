<?php

return [
    'owner'=>env('PRODUCT_OWNER', 'Demo Travel Agency LTD'),
    'author'=>'segsalerty@gmail.com',
    'description'=>env('PRODUCT_DESCRIPTION', ''),
    'layout'=>env('PRODUCT_LAYOUT', 1),
    'theme'=>env('PRODUCT_THEME', 'brick-red'),
    'currency_name'=>env('PRODUCT_CURRENCY_THEME', 'NGN'),
    'currency_code'=>env('PRODUCT_CURRENCY_THEME', '&#8358;'),
    'multi_currency'=>env('PRODUCT_MULTI_CURRENCY', true)
];