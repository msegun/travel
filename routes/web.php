<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/admin', function(){
    return redirect('admin/login');
});
Route::get('/admin/login', 'Admin\Auth\LoginController@index');
Route::post('/admin/login', 'Admin\Auth\LoginController@login');
Route::get('/admin/logout','Admin\Auth\LoginController@logout');

Auth::routes(); //all Auth routes register

Route::group([
    'middleware' => ['admin'],
    'prefix' => 'admin',
    'namespace' => 'Admin'
], function () {
    Route::get('/dashboard', 'DashboardController@index');
    Route::group([
        'prefix' => 'site'
    ], function (){
        Route::get('/profile', 'SiteController@profile');
        Route::post('/profile', 'SiteController@profile');
        Route::get('/content/{tab?}', 'SiteController@content');
        Route::post('/content/{tab?}', 'SiteController@content');
    });
});

Route::group([
    'middleware' => ['web'],
    'prefix' => 'customer',
    'guard' => 'auth'
], function(){
    Route::get('/home', 'HomeController@index');
});

Route::get('/setup', 'HomeController@setup');
